# set(MY_TOOLCHAIN_PATH /home/cyang/.toolchain/gcc-7.5.0)
SET(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
# SET(CMAKE_C_COMPILER   ${MY_TOOLCHAIN_PATH}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
# SET(CMAKE_CXX_COMPILER ${MY_TOOLCHAIN_PATH}/bin/arm-linux-gnueabihf-g++)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
