/**
 * 硬件的控制方法
 */
#ifndef __HANDLER_H__
#define __HANDLER_H__

#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <linux/input.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define LED_ON _IOW('l', 0, int) // 1开 2关
#define LED_OFF _IOW('l', 1, int)
#define FAN_ON _IO('f', 1)
#define FAN_OFF _IO('f', 0)
#define MOTOR_ON _IO('m', 1)
#define MOTOR_OFF _IO('m', 0)
#define BEEP_ON _IO('b', 1)
#define BEEP_OFF _IO('b', 0)

#define LED_FILE "/dev/led"

class Handler {
private:
  Handler();

public:
  ~Handler();
  static Handler *getHander();
  // 打开 LED
  int open_led(int index);
  // 关闭 LED
  int close_led(int index);
  // 打开蜂鸣器
  int open_beer(int level);
  // 关闭蜂鸣器
  // int close_beer(); // -> open_beer(0);
  // 打开马达
  int open_vibrator(int level);
  // 关闭马达
  // int close_motor();
  // 打开风扇
  int open_fan(int level);
  // 关闭风扇
  static int close_fan();
  // 读取温度
  // static int read_temp(char **temp); // -> humtemp.h/humtemp.cpp
  // 读取湿度
  // static int read_hum(char **hum);

private:
  int led_fd;
  int beer_fd;
  int fan_fd;
  int vibrator_fd;
  struct input_event beer_event;
  struct input_event vibrator_event;
  struct timeval beer_time;
  struct ff_effect vibrator_effect;
};

bool handler(char *payload);

#endif // !__HANDLER_H__
