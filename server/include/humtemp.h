#ifndef __HUMTEMP_H__
#define __HUMTEMP_H__

#include <arpa/inet.h> //大小端转换
#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/ipc.h> //共享内存
#include <sys/shm.h> //共享内存
#include <unistd.h>

#define GET_HUM _IOR('m', 1, int) // 获取湿度的功能码
#define GET_TEM _IOR('m', 0, int) // 获取温度的功能码

void my_get_hum_tem(); // 父进程打开iic文件读取温湿度

void my_put_hum_tem();// 子进程获取温湿度，显示在数码管上

int get_hum(int &hum); // 获取湿度

int get_temp(int &temp); // 获取温度

#endif