/**
 * 主程序是一个 MQTT 客户端，其主要的功能很简单：
 * 1. 通过订阅 broker 的指定 topic 来接收消息，
 *    依据消息调用相应的硬件功能；
 * 2. 定时向 broker 发送温湿度数据。
 * ==========================================
 * 主程序的客户端ID：board
 * broker 地址通过命令行参数传入
 * QOS 均为 1
 * 接收硬件控制指令以及发送温湿度数据的 topic：SMART_HOUSE
 * 详细的接口文档：https://node.notion.site/c47e0c36272e44b4ad10aaa6bf2cb701
 * ==========================================
 */
#include "handler.h"
#include <MQTTClient.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "humtemp.h"

#define QOS 1 // 优先级，0 最多一次，1 至少一次，2 恰好一次
#define TIMEOUT 5000L // 连接超时时间，由于发送间隔为 10 秒，超时时间小于 10
#define TOPIC "SMART_HOUSE" // 程序只在此 topic 收发消息

timer_t timerid;
volatile MQTTClient_deliveryToken deliveredtoken;
MQTTClient_message pubmsg;
MQTTClient client;
MQTTClient_deliveryToken token;

/**
 * 定时任务，这里自动发送温湿度数据
 */
void timer_handler(int signum) {
  int hum = 0; // 湿度
  char hum_str[6];
  int temp = 0; // 温度
  char temp_str[6];
  int rc;
  pubmsg.qos = QOS;
  pubmsg.retained = 0;

  get_hum(hum);
  snprintf(hum_str, sizeof(hum_str), "H%d", hum);
  printf("Humidity: %d\n", hum);

  rc = get_temp(temp);
  printf("Temperature: %d\n", temp);
  snprintf(temp_str, sizeof(temp_str), "T%d", temp);

  // 发送温度数据
  pubmsg.payload = (void *)temp_str;
  pubmsg.payloadlen = (int)strlen(temp_str);
  if ((rc = MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token)) !=
      MQTTCLIENT_SUCCESS) {
    printf("Failed to publish message, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  // 这里使用的不是异步的 paho 库，所以 public 是同步方法，需要等待
  // printf("Waiting for up to %d seconds for publication of %s\n"
  //        "on topic %s for client with ClientID: %s\n",
  //        (int)(TIMEOUT / 1000), "1234", "TEMP", "board");
  rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
  // printf("Message with delivery token %d delivered\n", token);

  // 发送湿度数据
  pubmsg.payload = (void *)hum_str;
  pubmsg.payloadlen = (int)strlen(hum_str);
  if ((rc = MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token)) !=
      MQTTCLIENT_SUCCESS) {
    printf("Failed to publish message, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
  printf("Message with delivery token %d delivered\n", token);

  struct itimerspec its;
  its.it_value.tv_sec = 10; // 10 秒后再次触发
  its.it_value.tv_nsec = 0;
  its.it_interval.tv_sec = 0;
  its.it_interval.tv_nsec = 0;
  timer_settime(timerid, 0, &its, NULL);
}

/**
 * 数据发送完毕后的回调函数，dt 为发送的字节长度
 */
void delivered(void *context, MQTTClient_deliveryToken dt) {
  // printf("Message with token value %d delivery confirmed\n", dt);
  deliveredtoken = dt; // 更新 id
}

/**
 * 数据接收到达时的回调函数，在这里处理 QT 发来的指令
 */
int msgarrvd(void *context, char *topicName, int topicLen,
             MQTTClient_message *message) {
  // 在这里获取到来自 Broker 的消息，调用对应的硬件
  // Broker 会把客户端自己发送的消息也发送过来，判断一下是不是自己发送的 TEMP 或者 HUM
  char *msg = (char *)message->payload;
  if (msg[0] != 'T' && msg[0] != 'H') { // 开头不是 T 或者 H 的是命令
    printf("recv: %s\n", msg);
    handler(msg);
  }
  // printf("Message arrived\n");
  // printf("     topic: %s\n", topicName);
  // printf("   message: %.*s\n", message->payloadlen, msg);
  // printf("   message: %.*s\n", message->payloadlen, msg);
  MQTTClient_freeMessage(&message);
  MQTTClient_free(topicName);
  return 1;
}

/**
 * 连接丢失时的回调函数，可用于尝试重新连接
 * 本项目暂时不进行处理，仅打印输出
 */
void connlost(void *context, char *cause) {
  printf("\nConnection lost\n");
  printf("     cause: %s\n", cause);
}

int main(int argc, char *argv[]) {
  pid_t pid;
  pid = fork();
  if (pid == 0) {
    // 这里调用 @ruiyang-wang 的数码管显示程序
    // https://gitee.com/ruiyang-wang/project-samrt-house/blob/80d0ba1d20d370ff2d198c38a5dd838051ee961a/driver/test/test.c
    int fd[2];
    pid_t pid2;
    pid2 = fork();
    if (pid2 < 0) {
      perror("fork");
      return -1;
    } else if (pid2 > 0) {
      my_get_hum_tem();
    } else if (pid2 == 0) {
      my_put_hum_tem();
    }
    return 0;
  } else if (pid > 0) { // 主进程开启一个 MQTT 客户端
    // MQTT 客户端基本信息
    char topic[] = "SMART_HOUSE";
    char clientid[] = "board";
    char *address;
    if (argc != 2) {
      printf("Usage: %s <address>\n", argv[0]);
      printf("example: %s tcp://192.168.1.210:1883\n", argv[0]);
      exit(1);
    }
    address = argv[1];

    pubmsg = MQTTClient_message_initializer;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;

    int rc;

    // 创建一个定时器，基于信号
    struct sigevent sev;
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = SIGALRM;
    sev.sigev_value.sival_ptr = &timerid;
    signal(SIGALRM, timer_handler);
    if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1) {
      perror("timer_create");
      exit(1);
    }

    // MQTT 客户端初始化
    if ((rc = MQTTClient_create(&client, address, clientid,
                                MQTTCLIENT_PERSISTENCE_NONE, NULL)) !=
        MQTTCLIENT_SUCCESS) {
      printf("Failed to create client, return code %d\n", rc);
      rc = EXIT_FAILURE;
      goto exit;
    }

    // MQTT 回调函数，用于处理数据到达情况
    if ((rc = MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd,
                                      delivered)) != MQTTCLIENT_SUCCESS) {
      printf("Failed to set callbacks, return code %d\n", rc);
      rc = EXIT_FAILURE;
      goto destroy_exit;
    }

    // MQTT 连接到 Broker
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;
    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
      printf("Failed to connect, return code %d\n", rc);
      rc = EXIT_FAILURE;
      goto destroy_exit;
    }

    // 调用定时器，开始发送温湿度数据
    struct itimerspec its;
    its.it_value.tv_sec = 10; // 10 秒后再次触发
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = 0;
    its.it_interval.tv_nsec = 0;
    timer_settime(timerid, 0, &its, NULL);

    // 订阅 SMART_HOUSE 主题，等待接收指令
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
           "Press Q<Enter> to quit\n\n",
           topic, clientid, QOS);
    if ((rc = MQTTClient_subscribe(client, topic, QOS)) != MQTTCLIENT_SUCCESS) {
      printf("Failed to subscribe, return code %d\n", rc);
      rc = EXIT_FAILURE;
    } else {
      int ch;
      do {
        ch = getchar();
      } while (ch != 'Q' && ch != 'q');

      if ((rc = MQTTClient_unsubscribe(client, topic)) != MQTTCLIENT_SUCCESS) {
        printf("Failed to unsubscribe, return code %d\n", rc);
        rc = EXIT_FAILURE;
      }
    }

    // 断开连接
    if ((rc = MQTTClient_disconnect(client, 10000)) != MQTTCLIENT_SUCCESS) {
      printf("Failed to disconnect, return code %d\n", rc);
      rc = EXIT_FAILURE;
    }
  destroy_exit:
    MQTTClient_destroy(&client);
  exit:
    return rc;
  } else {
    perror("fork");
    return -1;
  }
  return 0;
}
