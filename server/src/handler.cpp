#include "handler.h"
#include <cstring>
#include <linux/input-event-codes.h>
#include <linux/input.h>
#include <unistd.h>

// GPIO控制
#define LED_ON _IOW('l', 0, int) // 1开 2关
#define LED_OFF _IOW('l', 1, int)
#define FAN_ON _IO('f', 1)
#define FAN_OFF _IO('f', 0)
#define MOTOR_ON _IO('m', 1)
#define MOTOR_OFF _IO('m', 0)
#define BEEP_ON _IO('b', 1)
#define BEEP_OFF _IO('b', 0)

Handler::Handler() {
  this->led_fd = open("/dev/led", O_RDWR);
  if (this->led_fd < 0) {
    perror("open led");
    exit(1);
  }

  this->beer_fd = open("/dev/input/event0", O_RDWR);
  if (this->beer_fd < 0) {
    perror("open beer");
    exit(1);
  }
  this->beer_event.type = EV_SND;
  this->beer_event.code = SND_TONE;
  this->beer_event.value = 1000;
  this->beer_time.tv_sec = 1;
  this->beer_time.tv_usec = 0;
  this->beer_event.time = this->beer_time;

  this->vibrator_fd = open("/dev/input/event1", O_RDWR);
  if (this->vibrator_fd < 0) {
    perror("open vibrator");
    exit(1);
  }
  int num_effects;
  if (ioctl(this->vibrator_fd, EVIOCGEFFECTS, &num_effects) < 0) {
    perror("ioctl EVIOCGEFFECTS");
    exit(1);
  }
  this->vibrator_effect.type = FF_RUMBLE;
  this->vibrator_effect.id = -1;
  this->vibrator_effect.u.rumble.strong_magnitude = 0xFFFF;
  this->vibrator_effect.u.rumble.weak_magnitude = 0;
  this->vibrator_effect.replay.length = 3000;
  this->vibrator_effect.replay.delay = 0;
  if (ioctl(this->vibrator_fd, EVIOCSFF, &this->vibrator_effect) < 0) {
    perror("ioctl EVIOCSFF");
    exit(1);
  }
  // this->vibrator_event = {
  //   .type = EV_FF,
  //   .code = this->vibrator_effect.id,
  //   .value = 1,
  // };
  this->vibrator_event.type = EV_FF;
  this->vibrator_event.code = this->vibrator_effect.id;
  this->vibrator_event.value = 1;

  this->fan_fd = open("/sys/class/hwmon/hwmon1/pwm1", O_RDWR);
  if (this->fan_fd < 0) {
    perror("open fan");
    exit(1);
  }
}

Handler::~Handler() {
  close(this->led_fd);
  close(this->beer_fd);
  this->beer_event.code = SND_BELL;
  this->beer_event.value = 0000;
  close(fan_fd);
}

int Handler::open_led(int index) {
  // printf("open led %d\n", index);
  ioctl(this->led_fd, LED_ON, &index);
  return 0;
}

int Handler::close_led(int index) {
  // printf("close led %d\n", index);
  ioctl(this->led_fd, LED_OFF, &index);
  return 0;
}

int Handler::open_beer(int level) {
  if (level == 0) {
    this->vibrator_effect.u.rumble.strong_magnitude = 0x3FFF;
  }
  if (level == 1) {
    this->vibrator_effect.u.rumble.strong_magnitude = 0x5FFF;
  }
  if (level == 2) {
    this->vibrator_effect.u.rumble.strong_magnitude = 0x7FFF;
  }
  if (level == 3) {
    this->vibrator_effect.u.rumble.strong_magnitude = 0xFFFF;
  }
  this->vibrator_effect.u.rumble.weak_magnitude = 0;
  this->beer_event.value = level * 4000;
  int ret = write(this->beer_fd, &this->beer_event, sizeof(struct input_event));
  return 0;
}

int Handler::open_vibrator(int level) {
  this->vibrator_event.value = level * 4000;
  int ret = write(this->vibrator_fd, &this->vibrator_event,
                  sizeof(struct input_event));
  return 0;
}

int Handler::open_fan(int level) {
  char buf[4];
  int val = 0;
  if (level == 0)
    val = 0;
  if (level == 1)
    val = 220;
  if (level == 2)
    val = 230;
  if (level == 3)
    val = 240;
  snprintf(buf, sizeof(buf), "%d", val);
  if (write(fan_fd, buf, sizeof(buf)) < 0)
    return -1;
  return 0;
}

Handler *Handler::getHander() {
  static Handler handler;
  return &handler;
}

bool handler(char *payload) {
  Handler *handler = Handler::getHander();
  if (strcmp(payload, "101") == 0) // 关闭 LED1
    handler->close_led(1);
  if (strcmp(payload, "102") == 0) // 关闭 LED2
    handler->close_led(2);
  if (strcmp(payload, "103") == 0) // 关闭 LED2
    handler->close_led(3);
  if (strcmp(payload, "104") == 0) // 打开 LED1
    handler->open_led(1);
  if (strcmp(payload, "105") == 0) // 打开 LED2
    handler->open_led(2);
  if (strcmp(payload, "106") == 0) // 打开 LED3
    handler->open_led(3);
  if (strcmp(payload, "201") == 0) // 关闭风扇
    handler->open_fan(0);
  if (strcmp(payload, "202") == 0) // 开启风扇 1 档
    handler->open_fan(1);
  if (strcmp(payload, "203") == 0) // 开启风扇 2 档
    handler->open_fan(2);
  if (strcmp(payload, "204") == 0) // 开启风扇 3 档
    handler->open_fan(3);
  if (strcmp(payload, "205") == 0) // 开启风扇 4 档
    handler->open_fan(4);
  if (strcmp(payload, "301") == 0) // 关闭马达
    handler->open_vibrator(0);
  if (strcmp(payload, "302") == 0) // 开启马达 1 档
    handler->open_vibrator(1);
  if (strcmp(payload, "303") == 0) // 开启马达 2 档
    handler->open_vibrator(2);
  if (strcmp(payload, "304") == 0) // 开启马达 3 档
    handler->open_vibrator(3);
  if (strcmp(payload, "305") == 0) // 开启马达 4 档
    handler->open_vibrator(4);
  if (strcmp(payload, "401") == 0) // 关闭蜂鸣器
    handler->open_beer(0);
  if (strcmp(payload, "402") == 0) // 开启蜂鸣器 1 档
    handler->open_beer(1);
  if (strcmp(payload, "403") == 0) // 开启蜂鸣器 2 档
    handler->open_beer(2);
  if (strcmp(payload, "404") == 0) // 开启蜂鸣器 3 档
    handler->open_beer(3);
  if (strcmp(payload, "405") == 0) // 开启蜂鸣器 4 档
    handler->open_beer(4);
  return true;
}
