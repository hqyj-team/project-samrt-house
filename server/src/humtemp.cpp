#include "humtemp.h"

void my_get_hum_tem() // 父进程打开iic文件读取温湿度
{
  int tem, hum, fd0, tt, hh;
  float tem1, hum1;
  // 申请共享内存，温湿度的值存放在共享内存中
  int shmid = shmget((key_t)1234, 2 * sizeof(int), 0600 | IPC_CREAT);
  assert(shmid != -1);
  int *t = (int *)shmat(shmid, NULL, 0);
  assert(t != NULL);
  fd0 = open("/dev/si7006", O_RDWR);
  if (fd0 < 0) {
    printf("设备文件打开失败\n");
    exit(-1);
  }
  while (1) {
    // 获取数据
    ioctl(fd0, GET_HUM, &hum);
    ioctl(fd0, GET_TEM, &tem);
    // 大小端转换
    hum = ntohs(hum);
    tem = ntohs(tem);
    // 计算数据
    hum1 = 125.0 * hum / 65536 - 6;
    tem1 = 175.72 * tem / 65536 - 46.85;
    // printf("tem1 = %f  hum1 = %f \n", tem1, hum1);
    hh = (int)(hum1 * 100);
    tt = (int)(tem1 * 100);
    // 对共享内存区的变量赋值
    t[0] = hh;
    t[1] = tt;
    sleep(1);
  }
  shmdt(t);
  close(fd0);
}

void my_put_hum_tem() // 子进程获取温湿度，显示在数码管上
{
  int s, fd1;
  int tem, hum;
  // 申请共享内存，key值与主进程一致，若未申请则申请，已申请则使用
  int shmid = shmget((key_t)1234, 2 * sizeof(int), 0600 | IPC_CREAT);
  assert(shmid != -1);
  int *t = (int *)shmat(shmid, NULL, 0);
  assert(t != NULL);
  fd1 = open("/dev/m74hc595", O_RDWR);
  if (fd1 < 0) {
    printf("设备文件打开失败\n");
    exit(-1);
  }
  s = 60;
  while (1) // 循环显示温湿度
  {
    // 把共享内存区的值拿出来
    tem = t[0];
    hum = t[1];
    write(fd1, &tem, sizeof(int));
    write(fd1, &hum, sizeof(int));
  }
  shmdt(t);
  shmctl(shmid, IPC_RMID, NULL);
  close(fd1);
}

// 获取湿度
int get_hum(int &hum) {
  float hum1;
  int fd = open("/dev/si7006", O_RDWR);
  if (fd < 0) {
    printf("设备文件打开失败\n");
    exit(-1);
  }
  ioctl(fd, GET_HUM, &hum);
  hum = ntohs(hum);
  hum1 = 125.0 * hum / 65536 - 6;
  hum = hum1 * 100;
  close(fd);
  return 0;
}

int get_temp(int &temp) {
    float temp1;
  int fd = open("/dev/si7006", O_RDWR);
  if (fd < 0) {
    printf("设备文件打开失败\n");
    exit(-1);
  }
  ioctl(fd, GET_TEM, &temp);
  temp = ntohs(temp);
  temp1 = 175.72 * temp / 65536 - 46.85;
  temp = temp1 * 100;
  close(fd);
  return 0;
}
