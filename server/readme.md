## requirement
- cmake
- 需要将 paho.mqtt.c 库
    - arm 可以把 [这个编译好的库](https://alist.hqy.life/public/hqyj/%E9%A1%B9%E7%9B%AE%E6%96%87%E4%BB%B6/%E6%99%BA%E8%83%BD%E5%AE%B6%E5%B1%85%E7%B3%BB%E7%BB%9F/paho-arm.tar.xz) 覆盖到编译工具链和开发板的 rootfs
    - x86 我没有单独打包，请自己从源码库编译安装

## build(x86)
```bash
make x86
```
## build(arm)

确保 `arm-linux-gnueabihf-g++` 已经添加到环境变量

```bash
make arm
```

## Run

```bash
./bin/server
```

ARM: 复制到 rootfs 在开发板上运行
