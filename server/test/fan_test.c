#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define FAN_PWM_FILE "/sys/class/hwmon/hwmon1/pwm1"

int main() {
    int fd;
    char buf[10];
    int pwm_value;

    // 打开风扇PWM文件
    fd = open(FAN_PWM_FILE, O_RDWR);
    if (fd < 0) {
        perror("Failed to open fan PWM file");
        return 1;
    }

    // 读取当前PWM值
    if (read(fd, buf, sizeof(buf)) < 0) {
        perror("Failed to read fan PWM value");
        close(fd);
        return 1;
    }

    // 将读取到的字符串转换为整数
    pwm_value = atoi(buf);
    printf("Current fan PWM value: %d\n", pwm_value);

    // 设置新的PWM值
    printf("Enter new fan PWM value (0-255): ");
    scanf("%d", &pwm_value);

    // 将整数转换为字符串并写入PWM文件
    snprintf(buf, sizeof(buf), "%d", pwm_value);
    if (write(fd, buf, sizeof(buf)) < 0) {
        perror("Failed to write fan PWM value");
        close(fd);
        return 1;
    }

    printf("Fan PWM value set to: %d\n", pwm_value);

    // 关闭文件描述符
    close(fd);

    return 0;
}
