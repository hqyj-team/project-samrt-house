## 编译设备树

- 拷贝 dts 到 Linux 源码目录 arch/arm/boot/dts/stm32mp157a-fsmp1a.dts
- 在 Linux 源码根目录下

```bash
make dtb
```
- 拷贝 dtb 到 tftp 目录

```bash
cp arch/arm/boot/dts/stm32mp157a-fsmp1a.dtb ~/tftpboot
```

## 编译驱动

```
make arch=arm modname=mychrdev
```

- 拷贝驱动到rootfs
- 加载驱动模块

```bash
insmod mychrdev.ko
```
