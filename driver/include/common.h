#ifndef _COMMON_H_
#define _COMMON_H_


// GPIO控制
#define LED_ON    _IOW('l', 0, int) // 1开 2关
#define LED_OFF   _IOW('l', 1, int)
#define FAN_ON    _IO('f', 1)
#define FAN_OFF   _IO('f', 0)
#define MOTOR_ON  _IO('m', 1)
#define MOTOR_OFF _IO('m', 0)
#define BEEP_ON   _IO('b', 1)
#define BEEP_OFF  _IO('b', 0)

#endif //_COMMON_H_