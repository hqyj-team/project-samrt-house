#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/fs.h>
#include <linux/device.h>

#define GET_HUM _IOR('m', 1, int) // 获取湿度的功能码
#define GET_TEM _IOR('m', 0, int) // 获取温度的功能码

unsigned int major = 0;
struct class *cls;
struct device *dev;
struct i2c_client *client1;
// 读取温湿度的函数
int i2c_read_hum_tem(char reg)
{
    short value;
    char r_buf[] = {reg};
    int ret;
    // 封装消息
    struct i2c_msg r_msg[] = {
        [0] = {
            .addr = client1->addr,
            .flags = 0,
            .len = sizeof(r_buf),
            .buf = r_buf,
        },
        [1] = {
            .addr = client1->addr,
            .flags = 1,
            .len = 1,
            .buf = (char *)&value,
        },
    };
    // 将消息传送
    ret = i2c_transfer(client1->adapter, r_msg, 2);
    if (ret != 2)
    {
        printk("消息传输失败\n");
        return -EIO;
    }
    return value;
}

int si7006_open(struct inode *inode, struct file *file)
{
    printk("%s:%s:%d\n", __FILE__, __func__, __LINE__);
    return 0;
}
long si7006_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    int tem, hum, ret;
    switch (cmd)
    {
    case GET_HUM: // 读取湿度
        hum = i2c_read_hum_tem(0XE5);
        ret = copy_to_user((void *)arg, &hum, 4);
        if (ret)
        {
            printk("向用户拷贝数据失败\n");
            return ret;
        }
        break;
    case GET_TEM: // 读取温度
        tem = i2c_read_hum_tem(0XE3);
        ret = copy_to_user((void *)arg, &tem, 4);
        if (ret)
        {
            printk("向用户拷贝数据失败\n");
            return ret;
        }
        break;
    }
    return 0;
}
int si7006_close(struct inode *inode, struct file *file)
{
    printk("%s:%s:%d\n", __FILE__, __func__, __LINE__);
    return 0;
}
struct file_operations fops = {
    .open = si7006_open,
    .unlocked_ioctl = si7006_ioctl,
    .release = si7006_close,
};
// 给对象分配空间并且初始化
int i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    client1 = client;
    // 字符设备驱动的注册
    major = register_chrdev(0, "si7006", &fops);
    if (major < 0)
    {
        printk("字符设备驱动注册失败\n");
        return major;
    }
    printk("字符设备驱动注册成功\n");
    // 自动创建设备节点
    // 向上提交目录
    cls = class_create(THIS_MODULE, "si7006");
    if (IS_ERR(cls))
    {
        printk("向上提交目录失败\n");
        return -PTR_ERR(cls);
    }
    printk("向上提交目录成功\n");
    // 向上提交设备节点
    dev = device_create(cls, NULL, MKDEV(major, 0), NULL, "si7006");
    if (IS_ERR(dev))
    {
        printk("向上提交节点信息失败\n");
        return -PTR_ERR(dev);
    }
    printk("向上提交节点信息成功\n");
    return 0;
}

int i2c_remove(struct i2c_client *client)
{
    // 销毁节点
    device_destroy(cls, MKDEV(major, 0));
    // 销毁目录
    class_destroy(cls);
    // 注销字符设备驱动
    unregister_chrdev(major, "si7006");
    return 0;
}
// 定义设备树匹配的表
struct of_device_id oftable[] = {
    {
        .compatible = "wry,si7006",
    },
    {},
};
struct i2c_driver i2c_drv = {
    .probe = i2c_probe,
    .remove = i2c_remove,
    .driver = {
        .name = "si7006",
        .of_match_table = oftable,
    },
};
module_i2c_driver(i2c_drv);
MODULE_LICENSE("GPL");