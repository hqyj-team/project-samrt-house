#include "common.h"
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>

// GPIO子系统
struct device_node *dnode; // 设备树节点
struct gpio_desc *gpiono; // gpio编号

// 注册设备驱动
struct class *cls;
struct device *dev;
int major; // 用于保存主设备号

long fan_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    switch (cmd) {
    case FAN_ON: // 开风扇
        gpiod_set_value(gpiono, 1);
        break;
        break;
    case FAN_OFF: // 关风扇
        gpiod_set_value(gpiono, 0);
        break;
    }

    return 0;
}

// 操作方法结构体的初始化
struct file_operations fops = {
    .unlocked_ioctl = fan_ioctl,
};

// 安装驱动
static int __init fan_init(void)
{
    /* GPIO子系统 */

    // 1. 解析设备树节点名 fan
    dnode = of_find_node_by_path("/fan");
    if (!dnode) {
        printk(KERN_ALERT "dnode is null\n");
        return -ENXIO;
    }
    printk("设备树节点名:%s\n", dnode->name);

    // 2. 根据设备树节点解析fan1 gpio结构体并向内核注册
    gpiono = gpiod_get_from_of_node(dnode, "fan", 0, GPIOD_OUT_LOW, NULL);
    if (IS_ERR(gpiono)) {
        printk("申请gpio失败\n");
        return -PTR_ERR(gpiono);
    }

    /*  注册字符设备驱动 */
    major = register_chrdev(0, "fan", &fops);
    if (major < 0) {
        printk("字符设备驱动注册失败\n");
        return major;
    }
    printk("字符设备驱动注册成功major=%d\n", major);

    // 2. 向上提交目录
    cls = class_create(THIS_MODULE, "fan");
    if (IS_ERR(cls)) {
        printk("向上提交目录失败\n");
        return -PTR_ERR(cls);
    }
    printk("向上提交目录成功\n");

    // 3. 向上提交设备节点信息
    dev = device_create(cls, NULL, MKDEV(major, 0), NULL, "fan");
    if (IS_ERR(dev)) {
        printk("向上提交设备节点信息失败\n");
        return -PTR_ERR(dev);
    }
    printk("向上提交设备节点信息成功\n");

    return 0;
}

// 卸载驱动
static void __exit fan_exit(void)
{
    // 关风扇
    gpiod_set_value(gpiono, 0);
    // 注销gpio信息
    gpiod_put(gpiono);
    // 销毁设备节点信息
    device_destroy(cls, MKDEV(major, 0));
    class_destroy(cls);
    // 字符设备驱动的注销
    unregister_chrdev(major, "fan");
}

module_init(fan_init);
module_exit(fan_exit);
MODULE_LICENSE("GPL");