#include "common.h"
#include <linux/gpio.h>
#include <linux/gpio/consumer.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>

// GPIO子系统
struct device_node *dnode; // 设备树节点
struct gpio_desc *gpiono1, *gpiono2, *gpiono3; // gpio编号

// 注册设备驱动
struct class *cls;
struct device *dev;
int major; // 用于保存主设备号

long led_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    // 接收指针传参
    int which;
    int ret = copy_from_user(&which, (void *)arg, 4);
    if (ret) {
        printk("拷贝用户空间数据失败");
        return -EIO;
    }

    switch (cmd) {
    case LED_ON: // 开灯（ 指针传递第三个参数 ———— 常用！）
        switch (which) {
        case 1: // led1
            gpiod_set_value(gpiono1, 1);
            break;
        case 2: // led2
            gpiod_set_value(gpiono2, 1);
            break;
        case 3: // led3
            gpiod_set_value(gpiono3, 1);
            break;
        }
        break;
    case LED_OFF: // 关灯
        switch (which) {
        case 1: // led1
            gpiod_set_value(gpiono1, 0);
            break;
        case 2: // led2
            gpiod_set_value(gpiono2, 0);
            break;
        case 3: // led3
            gpiod_set_value(gpiono3, 0);
            break;
        }
        break;
    }

    return 0;
}

// 操作方法结构体的初始化
struct file_operations fops = {
    .unlocked_ioctl = led_ioctl,
};

// 安装驱动
static int __init led_init(void)
{
    /* GPIO子系统 */

    // 1. 解析设备树节点名 led
    dnode = of_find_node_by_path("/leds");
    if (!dnode) {
        printk(KERN_ALERT "dnode is null\n");
        return -ENXIO;
    }
    printk("设备树节点名:%s\n", dnode->name);

    // 2. 根据设备树节点解析led1 gpio结构体并向内核注册
    gpiono1 = gpiod_get_from_of_node(dnode, "led1", 0, GPIOD_OUT_LOW, NULL);
    if (IS_ERR(gpiono1)) {
        printk("申请gpio失败\n");
        return -PTR_ERR(gpiono1);
    }
    gpiono2 = gpiod_get_from_of_node(dnode, "led2", 0, GPIOD_OUT_LOW, NULL);
    if (IS_ERR(gpiono2)) {
        gpiod_put(gpiono1);
        printk("申请gpio失败\n");
        return -PTR_ERR(gpiono2);
    }
    gpiono3 = gpiod_get_from_of_node(dnode, "led3", 0, GPIOD_OUT_LOW, NULL);
    if (IS_ERR(gpiono3)) {
        gpiod_put(gpiono1);
        gpiod_put(gpiono2);
        printk("申请gpio失败\n");
        return -PTR_ERR(gpiono3);
    }

    /*  注册字符设备驱动 */
    major = register_chrdev(0, "led", &fops);
    if (major < 0) {
        printk("字符设备驱动注册失败\n");
        return major;
    }
    printk("字符设备驱动注册成功major=%d\n", major);

    // 2. 向上提交目录
    cls = class_create(THIS_MODULE, "led");
    if (IS_ERR(cls)) {
        printk("向上提交目录失败\n");
        return -PTR_ERR(cls);
    }
    printk("向上提交目录成功\n");

    // 3. 向上提交设备节点信息,三盏灯公用同一个设备文件
    dev = device_create(cls, NULL, MKDEV(major, 0), NULL, "led");
    if (IS_ERR(dev)) {
        printk("向上提交设备节点信息失败\n");
        return -PTR_ERR(dev);
    }
    printk("向上提交设备节点信息成功\n");

    return 0;
}

// 卸载驱动
static void __exit led_exit(void)
{
    // 灭灯
    gpiod_set_value(gpiono1, 0);
    gpiod_set_value(gpiono2, 0);
    gpiod_set_value(gpiono3, 0);
    // 注销gpio信息
    gpiod_put(gpiono1);
    gpiod_put(gpiono2);
    gpiod_put(gpiono3);
    // 销毁设备节点信息
    device_destroy(cls, MKDEV(major, 0));
    class_destroy(cls);
    // 字符设备驱动的注销
    unregister_chrdev(major, "led");
}

module_init(led_init);
module_exit(led_exit);
MODULE_LICENSE("GPL");