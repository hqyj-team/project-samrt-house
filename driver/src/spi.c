#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/fs.h>
#include <linux/device.h>
unsigned int major = 0;
struct class *cls;
struct device *dev;
struct i2c_client *client1;
struct spi_device *myspi;

int myspi_open(struct inode *inode, struct file *file)
{
    printk("%s:%s:%d\n", __FILE__, __func__, __LINE__);
    return 0;
}

ssize_t myspi_write(struct file *file, const char __user *ubuf, size_t size, loff_t *loff)
{
    int ret, s;
    int buf = 0;
    char abuf[2];
    char bbuf[2];
    char cbuf[2];
    char dbuf[2];
    unsigned int tab[10] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f};
    unsigned int tab1[10] = {0xbf, 0x86, 0xdb, 0xcf, 0xe6, 0xed, 0xfd, 0x87, 0xff, 0xef};
    unsigned int tac[4] = {0x01, 0x02, 0x04, 0x08};
    ret = copy_from_user(&buf, ubuf, size);
    if (ret)
    {
        printk("copy_from_user filed\n");
        return -EIO;
    }
    //第一位数码管
    abuf[0] = tac[0];
    abuf[1] = tab[buf / 1000];
    //第二位数码管
    bbuf[0] = tac[1];
    bbuf[1] = tab1[buf / 100 % 10];
    //第三位数码管
    cbuf[0] = tac[2];
    cbuf[1] = tab[buf / 10 % 10];
    //第四位数码管
    dbuf[0] = tac[3];
    dbuf[1] = tab[buf % 10];
    s = 50;
    while (s--)
    {
        spi_write(myspi, abuf, sizeof(abuf));
        spi_write(myspi, bbuf, sizeof(bbuf));
        spi_write(myspi, cbuf, sizeof(cbuf));
        spi_write(myspi, dbuf, sizeof(dbuf));
    }
    return 0;
}

int myspi_close(struct inode *inode, struct file *file)
{
    char buf[] = {0, 0};
    spi_write(myspi, buf, sizeof(buf));
    printk("%s:%s:%d\n", __FILE__, __func__, __LINE__);
    return 0;
}

struct file_operations fops = {
    .open = myspi_open,
    .write = myspi_write,
    .release = myspi_close,
};
int m74hc595_probe(struct spi_device *spi)
{
    myspi = spi;
    // 字符设备驱动的注册
    major = register_chrdev(0, "m74hc595", &fops);
    if (major < 0)
    {
        printk("字符设备驱动注册失败\n");
        return major;
    }
    printk("字符设备驱动注册成功\n");
    // 自动创建设备节点
    // 向上提交目录
    cls = class_create(THIS_MODULE, "m74hc595");
    if (IS_ERR(cls))
    {
        printk("向上提交目录失败\n");
        return -PTR_ERR(cls);
    }
    printk("向上提交目录成功\n");
    // 向上提交设备节点
    dev = device_create(cls, NULL, MKDEV(major, 0), NULL, "m74hc595");
    if (IS_ERR(dev))
    {
        printk("向上提交节点信息失败\n");
        return -PTR_ERR(dev);
    }
    printk("向上提交节点信息成功\n");

    return 0;
}
int m74hc595_remove(struct spi_device *spi)
{
    char buf[] = {0, 0};
    spi_write(myspi, buf, sizeof(buf));
    // 销毁节点
    device_destroy(cls, MKDEV(major, 0));
    // 销毁目录
    class_destroy(cls);
    // 注销字符设备驱动
    unregister_chrdev(major, "m74hc595");
    printk("%s:%d\n", __FILE__, __LINE__);
    return 0;
}
// 设备树匹配表
struct of_device_id of_table[] = {
    {.compatible = "wry,m74hc595"},
    {},
};
// 定义SPI对象并且初始化
struct spi_driver m74hc595 = {
    .probe = m74hc595_probe,
    .remove = m74hc595_remove,
    .driver = {
        .name = "m74hc595",
        .of_match_table = of_table,
    },
};
module_spi_driver(m74hc595);
MODULE_LICENSE("GPL");