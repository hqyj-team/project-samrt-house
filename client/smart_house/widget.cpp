﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //设置纯净窗口

    this->setWindowFlag(Qt::FramelessWindowHint);
    //窗体设置
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);//背景半透明属性设置
    this->setWindowFlags(Qt::FramelessWindowHint);//无边框窗体设置
    QRect rect = this->rect();
    QPainterPath painterPath;
    painterPath.addRoundedRect(rect, 15, 15);//15为圆角角度


    ui->progressBar->setAlignment(Qt::AlignLeft);
    mTime = new QTimer(this);
    mTime->setInterval(10);


    connect(mTime,&QTimer::timeout,this,&Widget::on_progress_Timeout);

    //将按钮发送的信号连接到自定义my_signal信号上
  //  connect(ui->pushButton,&QPushButton::clicked,this,&Widget::my_signal);
    //启动定时器
    mTime->start();
    QString styleSheet = "QProgressBar {"
                         "    border: 2px solid #E5E5E5;"
                         "    border-radius: 5px;"
                         "    background-color: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop:0 #E5E5E5, stop:1 #F7F7F7);"
                         "    text-align: center;"
                         "}"
                         "QProgressBar::chunk {"
                         "    background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop:0 #51C2D5, stop:0.5 #3A93AC, stop:1 #AEE6E8);"
                         "    width: 10px;"
                         "}";

    ui->progressBar->setStyleSheet(styleSheet);




}

Widget::~Widget()
{
    delete ui;
}



//时间
void Widget::on_progress_Timeout()
{
    int currentValue = ui->progressBar->value();
    if(currentValue >= ui->progressBar->maximum())
    {
        mTime->stop();
        //发射界面跳转信号
        emit jump();
        //关闭当前界面
        this->close();
    }
    else
    {
        ui->progressBar->setValue(ui->progressBar->value()+1);
    }

}
