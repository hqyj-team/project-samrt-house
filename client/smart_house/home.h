﻿#ifndef HOME_H
#define HOME_H

#include <QWidget>
#include <QMovie> //动图

//摄像头
#include <QCameraImageCapture>
#include <QCamera>
#include <QTimer>
#include<QDateTime>
#include<QTimerEvent>
#include <QMovie>
#include <QMessageBox>
#include <crtdefs.h>
#include<QDebug>
#include "qmqtt.h"
#include <QMainWindow>


//服务器头
#include <QWidget>
#include<QVector>               //定义容器，存放连接过来的客户端套接字
#include<QMessageBox>

using namespace QMQTT;

//协议宏
/*
#define LED1_OFF     "101"
#define LED2_OFF     "102"
#define LED3_OFF     "103"
#define LED1_ON      "104"
#define LED2_ON      "105"
#define LED3_ON      "106"
*/
#define LED1_OFF       "101"
#define LED2_OFF       "102"
#define LED3_OFF       "103"
#define LED1_ON        "104"
#define LED2_ON        "105"
#define LED3_ON        "106"

#define FAN_OFF       "201"
#define FAN_ON1       "202"
#define FAN_ON2       "203"
#define FAN_ON3       "204"

#define MOTOR_OFF     "301"
#define MOTOR_ON1     "302"
#define MOTOR_ON2     "303"
#define MOTOR_ON3     "304"

#define BUZZER_OFF    "401"
#define BUZZER_ON1    "402"
#define BUZZER_ON2    "403"
#define BUZZER_ON3    "404"

#define TEMPERATURE   "T30"
#define HUMIDNESS     "H60"
#define USERNAME      "admin"


namespace Ui {
class home;
}

class home : public QWidget
{
    Q_OBJECT

public:
    explicit home(QWidget *parent = nullptr);
    ~home();
    void receive_jump();  //接收信号的槽函数
    void timerEvent(QTimerEvent *e);           //重写定时器事件处理函数

private slots:
    void on_pushButton_2_clicked();//首页页面

    void on_pushButton_3_clicked();//客厅页面

    void on_pushButton_4_clicked();//卧室页面

    void on_pushButton_5_clicked();//监控页面

    void on_pushButton_6_clicked();//设置页面

    void on_pushButton_10_clicked();//确定

    void on_pushButton_7_clicked();//修改

    void on_pushButton_clicked();//拍照

    //void on_pushButton_9_clicked();//退出

    void on_pushButton_8_clicked();//风扇

    void on_pushButton_11_clicked();//马达

    void on_pushButton_12_clicked();//蜂鸣器

    void on_connected_slot();  //自定义处理connected的槽函数

    void on_readRead_slot();  //声明处理readRead信号的槽函数

    void on_pushButton_13_clicked();

    void on_pushButton_14_clicked();

    void on_pushButton_15_clicked();
private:
    Ui::home *ui;
    //摄像头对象
    QCamera *ca;
    QCameraImageCapture *capture;
    QMQTT::Client *client; //MQTT客户端指针
public:
    //定义客户端指针
    QTcpSocket *socket;

    //定义变量接收ui界面的用户名
    QString userName;    //很多成员函数都要用
    QString IP = "192.168.1.100";
    quint16 PORT = 6666;

public slots:
    void doConnected();  //MQTT 连接成功
    void doDisconnected();//MQTT连接断开
    void doDataReceived(QMQTT::Message);//MQTT收到数据

};

#endif // HOME_H
