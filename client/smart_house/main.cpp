#include "widget.h"
#include "home.h"


#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Widget w;
    w.show();

    //实例化第二个对象
    home s;
    //将第一个界面的跳转信号，链接到第二个界面对应的槽函数中

    QObject::connect(&w,&Widget::jump,&s,&home::receive_jump);

    return a.exec();
}
