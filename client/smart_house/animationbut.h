#ifndef ANIMATIONBUT_H
#define ANIMATIONBUT_H

#include <QWidget>
#include <QVariant>
#include <QEvent>

class QPropertyAnimation;

class Animationbut : public QWidget
{
    Q_OBJECT
public:
    explicit Animationbut(QWidget *parent = nullptr);

signals:

public:
    void enterEvent(QEvent *event);//鼠标进入
    void leaveEvent(QEvent *event);//鼠标移出
    void paintEvent(QPaintEvent *event);//绘制

private:
    bool enter;//是否进入
    bool leave;//是否移出
    int pixwidth;//图片宽度
    int pixheight;//图片高度
    int oldwidth;//图片旧宽度
    int oldheight;//图片旧高度

    int targetwidth;//目标宽度
    int targetheight;//目标高度
    QString text;//显示文字
    QString image;//图像路径




};

#endif // ANIMATIONBUT_H
