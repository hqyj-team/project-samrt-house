﻿#include "home.h"
#include "ui_home.h"
//摄像头头文件
#include <QCameraInfo>
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraImageCapture>
#include <QPixmap>
#include <QTcpSocket>
//弹窗
#include <QMessageBox>
static int i = 0;//风扇
static int m = 0;//马达
static int b = 0;//蜂鸣器
home::home(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::home)
{
    ui->setupUi(this);
    //////////////////////////////////////////////////////////////////////////


    //实例化客户端对象
    socket = new QTcpSocket(this);

    //将客户点发射的connected的信号与自定义槽函数连接
    connect(socket,&QTcpSocket::connected,this,&home::on_connected_slot);

    //当服务器向客户端发来消息时，该客户端就会自动发射一个readyRead的信号
    //我们可以在该信号对应的槽函数中处理对应逻辑
    connect(socket,&QTcpSocket::readyRead,this,&home::on_readRead_slot);
    //当客户端断开连接后，该客户端会发送一个disconnected信号
    //我们可以在该信号对应的槽函数中处理对应逻辑
    connect(socket,&QTcpSocket::disconnected,[&](){
        QMessageBox::information(this,"消息","断开成功");
    });
//////////////////////////////////////////////////////////////////////////////////////
    //启动一个定时器，每隔十分之一秒秒钟超时一次
    this->startTimer(100);


    //获取可用摄像头设备并输出在控制台
    QList<QCameraInfo> infos = QCameraInfo::availableCameras();
    qDebug() << infos.value(0).deviceName() << ":" <<infos.value(0).description();
    QString camera = infos.value(0).deviceName();
    qDebug() << camera;
    //显示摄像头
    ca =new QCamera(camera.toUtf8() ,this );
    ui->widget0->show();

    QCameraViewfinder *v2 = new QCameraViewfinder(ui->widget0);
    v2->resize(ui->widget0->size());
    ca->setViewfinder(v2);
    v2->show();
//////////////////////////////////////////////////////////////////////////////////////////
    //设置标签
    this->setWindowTitle("全自动智能家居");        //设置窗口标题
    this->setWindowIcon(QIcon(":/new/prefix1/img/home.png"));

    //设置窗口固定大小
    this->setFixedSize(this->size());

    //设置背景A
    ui->frame->setStyleSheet("background-color:skyblue;");
    //设置背景
    ui->label->setPixmap(QPixmap(":/new/prefix1/img/bj1.png"));
    ui->label->setScaledContents(true);

    //首页背景
    QMovie *movie=new QMovie(":/new/prefix1/img/R-C.gif");
    //ui->label_7->setMovie(movie);
    movie->start();
    //ui->label_7->setScaledContents(true);

    //客厅背景
    ui->label_2->setPixmap(QPixmap(":/new/prefix1/img/bj2.png"));
    ui->label_2->setScaledContents(true);

    //卧室背景
    ui->label_3->setPixmap(QPixmap(":/new/prefix1/img/bj2.png"));
    ui->label_3->setScaledContents(true);

    //设置背景
    ui->label_5->setPixmap(QPixmap(":/new/prefix1/img/bj1.png"));
    ui->label_5->setScaledContents(true);

    //监控背景
    ui->label_11->setPixmap(QPixmap(":/new/prefix1/img/bj1.png"));
    ui->label_11->setScaledContents(true);

    //设置按钮透明
   // ui->pushButton_9->setFlat(true);
    //设置按钮颜色
    ui->pushButton_7->setStyleSheet("background-color:skyblue;");
    ui->pushButton_10->setStyleSheet("background-color:skyblue;");
    ui->pushButton_13->setStyleSheet("background-color:skyblue;");
    ui->pushButton_14->setStyleSheet("background-color:skyblue;");
    ui->pushButton_15->setStyleSheet("background-color:skyblue;");

    //灯图标
    ui->label_16->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
    ui->label_16->setScaledContents(true);
    ui->label_17->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
    ui->label_17->setScaledContents(true);
    ui->label_18->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
    ui->label_18->setScaledContents(true);





//////////////////////////////////////////////////////////////////////////////////

    //设置风扇按钮
    QString imgPathStr = ":/new/prefix1/img/fan.png"; // 背景图片文件路径
    QString styleSheet = QString("QPushButton{border-image: url(%1);}").arg(imgPathStr);
    QPushButton *btn = ui->pushButton_8;
    btn->setStyleSheet(styleSheet);
    QImage img(imgPathStr);
    int w = img.width();
    int h = img.height();  // 图片宽高等比例缩放
    int maxSide = 80;     // 调整图片中宽高最大者至maxSide
    if(w >= h){
        double scale = maxSide / double(w);
        w = maxSide;
        h *= scale;
    }else{
        double scale = maxSide / double(h);
        h = maxSide;
        w *= scale;
    }
    btn->setFixedSize(w,h);


    //设置马达按钮
    QString imgPathStr1 = ":/new/prefix1/img/motor.png"; // 背景图片文件路径
    QString styleSheet1 = QString("QPushButton{border-image: url(%1);}").arg(imgPathStr1);
    QPushButton *btn1 = ui->pushButton_11;
    btn1->setStyleSheet(styleSheet1);
    QImage img1(imgPathStr1);
    int w1 = img.width();
    int h1 = img.height();  // 图片宽高等比例缩放
    int maxSide1 = 80;     // 调整图片中宽高最大者至maxSide
    if(w1 >= h1){
        double scale = maxSide1 / double(w1);
        w1 = maxSide1;
        h1 *= scale;
    }else{
        double scale = maxSide1 / double(h1);
        h1 = maxSide1;
        w1 *= scale;
    }
    btn1->setFixedSize(w1,h1);

    //设置蜂鸣器按钮
    QString imgPathStr2 = ":/new/prefix1/img/buzzer.png"; // 背景图片文件路径
    QString styleSheet2 = QString("QPushButton{border-image: url(%1);}").arg(imgPathStr2);
    QPushButton *btn2 = ui->pushButton_12;
    btn2->setStyleSheet(styleSheet2);
    QImage img2(imgPathStr2);
    int w2 = img.width();
    int h2 = img.height();  // 图片宽高等比例缩放
    int maxSide2 = 80;     // 调整图片中宽高最大者至maxSide
    if(w2 >= h2){
        double scale = maxSide2 / double(w2);
        w2 = maxSide2;
        h2 *= scale;
    }else{
        double scale = maxSide2 / double(h2);
        h2 = maxSide2;
        w2 *= scale;
    }
    btn2->setFixedSize(w2,h2);
//////////////////////////////////////////////////////////////////////////////////////////////
    //设置文本框
    //温湿度字体可获取
    ui->lineEdit_5->setReadOnly(false);
    ui->lineEdit_6->setReadOnly(false);
    ui->lineEdit_5->setStyleSheet("background:transparent;border-width:0;border-style:outset");
    ui->lineEdit_6->setStyleSheet("background:transparent;border-width:0;border-style:outset");


    //温湿度字体颜色
    QLCDNumber *m_pLcdTime = ui->lcdNumber_2;
    m_pLcdTime->setSegmentStyle(QLCDNumber::Flat);
    //调色板
    QPalette lcdpat = m_pLcdTime->palette();
    /*设置颜色，整体背景颜色 颜色蓝色,此函数的第一个参数可以设置多种。如文本、按钮按钮文字、多种*/
    lcdpat.setColor(QPalette::Normal,QPalette::WindowText,Qt::red);
    //设置当前窗口的调色板
    m_pLcdTime->setPalette(lcdpat);

    QLCDNumber *m_pLcdTime1 = ui->lcdNumber_3;
    m_pLcdTime1->setSegmentStyle(QLCDNumber::Flat);
    //调色板
    QPalette lcdpat1 = m_pLcdTime1->palette();
    /*设置颜色，整体背景颜色 颜色蓝色,此函数的第一个参数可以设置多种。如文本、按钮按钮文字、多种*/
    lcdpat1.setColor(QPalette::Normal,QPalette::WindowText,Qt::red);
    //设置当前窗口的调色板
    m_pLcdTime1->setPalette(lcdpat1);
   // client = new QMQTT::Client(QHostAddress("192.168.250.100"),1883);
    client = new QMQTT::Client(QHostAddress("192.3.98.64"),1883);
    client->setClientId("clientid");
    client->setUsername("user");
    client->setPassword("password");
    client->connectToHost();

    connect(client,SIGNAL(connected()),this,SLOT(doConnected()));
    connect(client,SIGNAL(disconnected()),this,SLOT(doDisconnected()));
    connect(client,SIGNAL(received(QMQTT::Message)),this,SLOT(doDataReceived(QMQTT::Message)));



    //修改挡位显示
    QFont font("Arial",12);
    ui->label_8->setFont(font);

    ui->label_20->setFont(font);

    ui->label_21->setFont(font);

    //设置不允许修改
    ui->lineEdit->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
}

home::~home()
{
    //告诉服务器，我走了
    QString msg = userName + ":离开聊天室";
    socket->write(msg.toLocal8Bit());

    //断开功能
    socket->disconnectFromHost();  //断开客户端和服务器的连接
    delete ui;
}

void home::doConnected()
{
    client->subscribe("SMART_HOUSE");
    qDebug()<<"connect sucess";
  //  ui->rb_status->setChecked(true);
  //  ui->pb_connect->setText("Disconnect");
  //  ui->pushButton->setEnabled(true);
  //  ui->pushButton_2->setEnabled(true);
}

void home::doDisconnected()
{
  //  ui->rb_status->setChecked(false);
  //  ui->pb_connect->setText("Connect");
  //  ui->pushButton->setEnabled(false);
  //  ui->pushButton_2->setEnabled(false);
}

void home::doDataReceived(Message message)
{

    //topic:话题  payload：数据
    QString mes = QString(message.id())+" "+QString(message.qos())+" "+message.topic()+" "+message.payload()+"\n";
    qDebug()<<message.payload();
    //ui->te_log->append(mes);
    QString yz_temp = ui->lineEdit_5->text();
    QString yz_hum = ui->lineEdit_6->text();
    if(message.payload().at(0) == 'T') //在这个IF条件下刷新温度
    {
        QString recv_msg= message.payload().right(4);

        ui->lcdNumber_2->display(recv_msg.insert(2,'.'));
        qDebug()<<yz_temp.toFloat();
        if(recv_msg.toFloat() - yz_temp.toFloat() > 0)
        {
            qDebug()<< "超过阈值";
            QString topic = "SMART_HOUSE";
            QString payload = FAN_ON2;
            ui->label_8->setText("2档");
            i=2;
            QMQTT::Message message(1,topic,payload.toUtf8());
            client->publish(message);
        }
        else
        {
            qDebug()<<"低于阈值";
           // QString topic = "SMART_HOUSE";
           // QString payload = FAN_OFF;
          // QMQTT::Message message(1,topic,payload.toUtf8());
         //  client->publish(message);
        }
        qDebug()<<recv_msg;
    }
    else if(message.payload().at(0) == 'H')
    {
        QString recv_msg= message.payload().right(4);
        qDebug()<<recv_msg;
        ui->lcdNumber_3->display(recv_msg.insert(2,'.'));
        if(recv_msg.toFloat() - yz_hum.toFloat() > 0)
        {
            qDebug()<< "超过阈值";
            QString topic = "SMART_HOUSE";
            QString payload = MOTOR_ON1;
            ui->label_20->setText("1档");
            m=1;
            QMQTT::Message message(1,topic,payload.toUtf8());
            client->publish(message);
        }
        else
        {
            qDebug()<<"低于阈值";
            QString topic = "SMART_HOUSE";
            QString payload = MOTOR_OFF;
            m=0;
            QMQTT::Message message(1,topic,payload.toUtf8());
            client->publish(message);
        }
        qDebug()<<recv_msg;
    }

    qDebug()<<mes;
}

//接收信号
void home::receive_jump()
{
    this->show();
    //显示首页
    ui->stackedWidget->setCurrentIndex(0);
    //连接功能
    QString ip = IP; //获取主机地址
    quint16 port = PORT;  //获取端口号
    socket->connectToHost(ip,port);

    //发送阈值
    QString msg = TEMPERATURE;
    socket->write(msg.toLocal8Bit());
    msg = HUMIDNESS;
    socket->write(msg.toLocal8Bit());

}

//首页按钮槽函数
void home::on_pushButton_2_clicked()
{
    //显示对应界面
    ui->stackedWidget->setCurrentIndex(0);
    //关闭摄像头
    ca->stop();
}

//客厅槽函数
void home::on_pushButton_3_clicked()
{
    //显示对应界面
    ui->stackedWidget->setCurrentIndex(1);

    //禁用阈值
   // ui->lineEdit_5->setEnabled(false);
   // ui->lineEdit_6->setEnabled(false);

    //关闭摄像头
    ca->stop();
}

//卧室槽函数
void home::on_pushButton_4_clicked()
{
    //显示对应界面
    ui->stackedWidget->setCurrentIndex(2);
    //关闭摄像头
    ca->stop();
}

//监控槽函数
void home::on_pushButton_5_clicked()
{
    //显示对应界面
    ui->stackedWidget->setCurrentIndex(3);
    //打开摄像头
    ca->start();

}

//设置槽函数
void home::on_pushButton_6_clicked()
{
    //显示对应界面
    ui->stackedWidget->setCurrentIndex(4);
    ui->lineEdit->setText("192.3.98.64");
    ui->lineEdit_2->setText("1883");
    QString port = QString::number(PORT);

    ui->lineEdit->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
    //关闭摄像头
    ca->stop();
}


//修改确定按钮槽函数
void home::on_pushButton_10_clicked()
{
    //关闭之前的连接
    client->unsubscribe("SMART_HOUSE");
    client->disconnect();

    //新的连接
    client = new QMQTT::Client(QHostAddress(ui->lineEdit->text()),atoi(ui->lineEdit_2->text().toStdString().c_str()));
    client->setClientId("clientid");
    client->setUsername("user");
    client->setPassword("password");
    client->connectToHost();

    connect(client,SIGNAL(connected()),this,SLOT(doConnected()));
    connect(client,SIGNAL(disconnected()),this,SLOT(doDisconnected()));
    connect(client,SIGNAL(received(QMQTT::Message)),this,SLOT(doDataReceived(QMQTT::Message)));

    ui->lineEdit->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);

}

//修改按钮槽函数
void home::on_pushButton_7_clicked()
{
    ui->lineEdit->setEnabled(true);
    ui->lineEdit_2->setEnabled(true);
}

//拍照按钮槽函数
void home::on_pushButton_clicked()
{
    //截取用户打卡时的图片
    capture = new QCameraImageCapture(ca);
    capture->capture("C:\\Users\\15831\\Desktop\\home\\img");
    QMessageBox::information(this, "good", "good！");
}

//定时器事件处理函数的实现
void home::timerEvent(QTimerEvent *e)
{
    //获取系统日期时间
    QDateTime sysTime = QDateTime::currentDateTime();

    //将QDateTime类型数据转换为字符串
    QString time = sysTime.toString("hh:mm:ss");
    QString date = sysTime.toString("yyyy:MM:dd:ddd");
    //qDebug()<<time;
    //qDebug()<<date;

    QStringList timeList1 = time.split(":");
    QStringList timeList2 = date.split(":");

    //定义字符串，保存时间
    QString t = timeList1[0]+timeList1[1];

    //定义时间显示RZ
    ui->lcdNumber->setDigitCount(8);
    ui->lcdNumber->display(time);

    ui->label_9->setText(timeList2[0]+"年"+timeList2[1]+"月"+timeList2[2]+"日"+"  "+timeList2[3]);
    ui->label_9->setAlignment(Qt::AlignCenter);

    //获取套接字中的数据
    QByteArray msg = socket->readAll();
    QByteArray subData = msg.mid(0, 4);

    QByteArray subData1 = msg.mid(4, 4);
//   qDebug() << subData;
    //展示到ui界面上
    if(!subData.isNull())
    {
         ui->lcdNumber_2->display(QString::fromLocal8Bit(subData));
         ui->lcdNumber_3->display(QString::fromLocal8Bit(subData1));
    }
    int wen = subData.toInt();
    int shi = subData1.toInt();
    if(wen >30 && i ==0)
    {
        QString msg = FAN_ON3;
        socket->write(msg.toLocal8Bit());
        if(b == 0)
        {
             msg = BUZZER_ON3;
             socket->write(msg.toLocal8Bit());
        }
    }
    if(shi > 80 && m == 0)
    {
        QString msg = MOTOR_ON3;
        socket->write(msg.toLocal8Bit());
    }

}



//风扇
void home::on_pushButton_8_clicked()
{
    QString msg;
    if(i == 0)
    {
        ui->label_8->setText("1档");
        QString topic = "SMART_HOUSE";
        QString payload = FAN_ON1;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = FAN_NO1;
        //socket->write(msg.toLocal8Bit());
        i++;
    }else if(i == 1)
    {
        ui->label_8->setText("2档");
        QString topic = "SMART_HOUSE";
        QString payload = FAN_ON2;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = FAN_NO2;
        //socket->write(msg.toLocal8Bit());
        i++;
    }else if(i == 2)
    {
        ui->label_8->setText("3档");
        QString topic = "SMART_HOUSE";
        QString payload = FAN_ON3;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = FAN_NO3;
        //socket->write(msg.toLocal8Bit());
        i++;
    }else if(i == 3)
    {
        ui->label_8->setText("空档");
        QString topic = "SMART_HOUSE";
        QString payload = FAN_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = FAN_OFF;
        //socket->write(msg.toLocal8Bit());
        i = 0;
    }
}

//马达
void home::on_pushButton_11_clicked()
{
    QString msg;

    if(m == 0)
    {
        ui->label_20->setText("1档");
        QString topic = "SMART_HOUSE";
        QString payload = MOTOR_ON1;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = MOTOR_NO1;
        //socket->write(msg.toLocal8Bit());
        m++;
    }else if(m == 1)
    {
        ui->label_20->setText("2档");
        QString topic = "SMART_HOUSE";
        QString payload = MOTOR_ON2;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = MOTOR_NO2;
        //socket->write(msg.toLocal8Bit());
        m++;
    }else if(m == 2)
    {
        ui->label_20->setText("3档");
        QString topic = "SMART_HOUSE";
        QString payload = MOTOR_ON3;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = MOTOR_NO3;
        //socket->write(msg.toLocal8Bit());
        m++;
    }else if(m == 3)
    {
        ui->label_20->setText("空档");
        QString topic = "SMART_HOUSE";
        QString payload = MOTOR_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = MOTOR_OFF;
        //socket->write(msg.toLocal8Bit());
        m = 0;
    }
}

//蜂鸣器
void home::on_pushButton_12_clicked()
{
    QString msg;

    if(b == 0)
    {
        ui->label_21->setText("1档");
        QString topic = "SMART_HOUSE";
        QString payload = BUZZER_ON1;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = BUZZER_NO1;
        //socket->write(msg.toLocal8Bit());
        b++;
    }else if(b == 1)
    {
        ui->label_21->setText("2档");
        QString topic = "SMART_HOUSE";
        QString payload = BUZZER_ON2;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = BUZZER_NO2;
        //socket->write(msg.toLocal8Bit());
        b++;
    }else if(b == 2)
    {
        ui->label_21->setText("3档");
        QString topic = "SMART_HOUSE";
        QString payload = BUZZER_ON3;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = BUZZER_NO3;
        //socket->write(msg.toLocal8Bit());
        b++;
    }else if(b == 3)
    {
        ui->label_21->setText("空档");
        QString topic = "SMART_HOUSE";
        QString payload = BUZZER_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);
        //msg = BUZZER_OFF;
        //socket->write(msg.toLocal8Bit());
        b = 0;
    }
}

//自定义处理connected的槽函数
void home::on_connected_slot()
{
    //给服务器发送数据，告诉服务器我来了

    QString msg = "admian:进入聊天室";

    //将该消息发送给服务器
    socket->write(msg.toLocal8Bit());
}

//声明处理readRead信号的槽函数
void home::on_readRead_slot()
{
    //获取套接字中的数据
   // QByteArray msg = socket->readAll();

    //展示到ui界面上
   // ui->lcdNumber_2->display(QString::fromLocal8Bit(msg));
}

//左床头灯
void home::on_pushButton_13_clicked()
{
    QString msg;
    static int z = 0;
    if(z == 0)
    {
        ui->pushButton_13->setText("关闭左床头灯");
        ui->label_16->setPixmap(QPixmap(":/new/prefix1/img/lamp1.png"));
        ui->label_16->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED1_ON;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

        //msg = LED1_NO;
        //socket->write(msg.toLocal8Bit());
        z++;

    }else if(z == 1)
    {
        ui->pushButton_13->setText("左床头灯");
        ui->label_16->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
        ui->label_16->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED1_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

        //msg = LED1_OFF;
        //socket->write(msg.toLocal8Bit());
        z = 0;
    }
}

//右床头灯
void home::on_pushButton_14_clicked()
{
    QString msg;
    static int y = 0;
    if(y == 0)
    {
        ui->pushButton_14->setText("关闭右床头灯");
        ui->label_17->setPixmap(QPixmap(":/new/prefix1/img/lamp1.png"));
        ui->label_17->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED2_ON;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

        // msg = LED2_NO;
        //socket->write(msg.toLocal8Bit());
        y++;
    }else if(y == 1)
    {
        ui->pushButton_14->setText("右床头灯");
        ui->label_17->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
        ui->label_17->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED2_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

        //msg = LED2_OFF;
        //socket->write(msg.toLocal8Bit());
        y = 0;
    }
}

//主灯
void home::on_pushButton_15_clicked()
{
    QString msg;
    static int u = 0;
    if(u == 0)
    {
        ui->pushButton_15->setText("关闭主灯");
        ui->label_18->setPixmap(QPixmap(":/new/prefix1/img/lamp1.png"));
        ui->label_18->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED3_ON;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

       // msg = LED3_NO;
       // socket->write(msg.toLocal8Bit());
        u++;
    }else if(u == 1)
    {
        ui->pushButton_15->setText("主灯");
        ui->label_18->setPixmap(QPixmap(":/new/prefix1/img/lamp.png"));
        ui->label_18->setScaledContents(true);

        QString topic = "SMART_HOUSE";
        QString payload = LED3_OFF;
        QMQTT::Message message(1,topic,payload.toUtf8());
        client->publish(message);

      //  msg = LED3_OFF;
      //  socket->write(msg.toLocal8Bit());
        u = 0;
    }
}

