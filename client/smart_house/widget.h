﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QString>
#include "qmqtt.h"
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
signals:
    //定义自己的信号函数
    void my_signal();   //自定义无参信号函数

    void my_signal1(QString msg,int num);

    void jump();  //页面跳转信号

private slots:
    void on_progress_Timeout();


private:
    Ui::Widget *ui;
     QTimer *mTime;//定时器
};
#endif // WIDGET_H
